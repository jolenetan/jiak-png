$(document).ready(function(){

  var getStep = window.location.href.split('#'); // find the step after '#'
  if(getStep[1] == 'step-2'){
    clickStep2();
  }

  $('.step-2').click(function() {
    clickStep2();
  });

  // action when trigger step 2
  function clickStep2(){
    $('html, body').animate({
        scrollTop: eval($("#booknow-form").offset().top -120)
    }, 1000);
    $('#step-1').addClass("disabled");
    $('#step-2').removeClass("disabled");
    $('.step-2').addClass("btn-primary");
    $('.step-2').removeClass("btn-default");
    $('#step-3').addClass("disabled");
    $('.step-3').removeClass("btn-primary");
    $('.step-3').addClass("btn-default");
    $('#step-4').addClass("disabled");
    $('.step-4').removeClass("btn-primary");
    $('.step-4').addClass("btn-default");
  }

  $('.step-3').click(function() {
    $('html, body').animate({
        scrollTop: eval($("#booknow-form").offset().top -120)
    }, 1000);
    $('#step-2').addClass("disabled");
    $('.step-2').addClass('btn-primary');
    $('#step-3').removeClass("disabled");
    $('.step-3').addClass("btn-primary");
    $('.step-3').removeClass("btn-default");
    $('#step-4').addClass("disabled");
    $('.step-4').removeClass("btn-primary");
    $('.step-4').addClass("btn-default");
    $('#step-1').addClass("disabled");
    document.getElementById('displayfinalsubtotal').innerHTML = finalsubtotal;
    document.getElementById('displayfinaldiscount').innerHTML = finaldiscount;
    document.getElementById('displayfinaltotal').innerHTML = "$" + finaltotal;
    document.getElementById('displayfinalpromocode').innerHTML = promocode;
    if(show == true) {
      $('#discountfinal').show();
    }
  });

  $('.step-4').click(function() {
    $('html, body').animate({
        scrollTop: eval($("#booknow-form").offset().top -120)
    }, 1000);
    $('.step-2').addClass('btn-primary');
    $('#step-3').addClass("disabled");
    $('.step-3').addClass('btn-primary');
    $('#step-4').removeClass("disabled");
    $('.step-4').addClass("btn-primary");
    $('.step-4').removeClass("btn-default");
    $('#step-1').addClass("disabled");
    $('#step-2').addClass("disabled");
  });

  $('.step-1').click(function() {
    $('html, body').animate({
        scrollTop: eval($("#booknow-form").offset().top -120)
    }, 1000);
    $('#step-1').removeClass('disabled');
    $('#step-2').addClass("disabled");
    $('#step-3').addClass("disabled");
    $('#step-4').addClass("disabled");
    $('.step-2').addClass("btn-default");
    $('.step-2').removeClass("btn-primary");
    $('.step-3').addClass("btn-default");
    $('.step-3').removeClass("btn-primary");
    $('.step-4').addClass("btn-default");
    $('.step-4').removeClass("btn-primary");
  });

});

$(function() {
  function c() {
    p();
    var e = h();
    var r = 0;
    var u = false;
    l.empty();
    while (!u) {
      if (s[r] == e[0].weekday) {
        u = true;
      } else {
        l.append('<div class="blank"></div>');
        r++;
      }
    }
    for (var c = 0; c < 42 - r; c++) {
      if (c >= e.length) {
        l.append('<div class="blank"></div>');
      } else {
        var v = e[c].day;
        var m = g(new Date(t, n - 1, v)) ? '<div class="today date-calendar-'+c+'" onclick="datepicker('+c+')">' : "<div onclick='datepicker("+c+")' class='date-calendar-"+c+"'><p>";
        l.append(m + "" + v + "</p></div>");
        // $('.date-calendar').click( datepicker(c) );
      }
    }
    var y = o[n - 1];
    a
      .css("background-color", y)
      .find("h1")
      .text(i[n - 1] + " " + t);
    f.find("div").css("color", y);
    l.find(".today").css("background-color", y);
    d();
  }

  function h() {
    var e = [];
    for (var r = 1; r < v(t, n) + 1; r++) {
      e.push({ day: r, weekday: s[m(t, n, r)] });
    }
    return e;
  }
  function p() {
    f.empty();
    for (var e = 0; e < 7; e++) {
      f.append("<div>" + s[e].substring(0, 3) + "</div>");
    }
  }
  function d() {
    var t;
    var n = $("#calendar").css("width", e + "px");
    n
      .find((t = "#calendar_weekdays, #calendar_content"))
      .css("width", e + "px")
      .find("div")
      .css({
        width: e / 7 + "px",
        height: e / 7 + "px",
        "line-height": e / 7 + "px"
      });
    n
      .find("#calendar_header")
      .css({ height: e * (1 / 7) + "px" })
      .find('i[class^="fas"]')
      .css("line-height", e * (1 / 7) + "px");
  }
  function v(e, t) {
    return new Date(e, t, 0).getDate();
  }
  function m(e, t, n) {
    return new Date(e, t - 1, n).getDay();
  }
  function g(e) {
    return y(new Date()) == y(e);
  }
  function y(e) {
    return e.getFullYear() + "/" + (e.getMonth() + 1) + "/" + e.getDate();
  }
  function b() {
    var e = new Date();
    t = e.getFullYear();
    n = e.getMonth() + 1;
  }
  var e = 480;
  var t = 2013;
  var n = 9;
  var r = [];
  var i = [
    "JANUARY",
    "FEBRUARY",
    "MARCH",
    "APRIL",
    "MAY",
    "JUNE",
    "JULY",
    "AUGUST",
    "SEPTEMBER",
    "OCTOBER",
    "NOVEMBER",
    "DECEMBER"
  ];
  var s = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
  ];
  var o = [
    "#e0402f"
  ];
  var u = $("#calendar");
  var a = u.find("#calendar_header");
  var f = u.find("#calendar_weekdays");
  var l = u.find("#calendar_content");
  b();
  c();
  a.find('i[class^="fas"]').on("click", function() {
    var e = $(this);
    var r = function(e) {
      n = e == "next" ? n + 1 : n - 1;
      if (n < 1) {
        n = 12;
        t--;
      } else if (n > 12) {
        n = 1;
        t++;
      }
      c();
    };
    if (e.attr("class").indexOf("left") != -1) {
      r("previous");
    } else {
      r("next");
    }
  });


});

  // $('.tour-results').hide();
  function datepicker(c) {
    $('.date-calendar-'+c).toggleClass("today");
    // $('.tour-results').toggle();
    // $('.tour-results-init').toggle();

  }

  function displaysubtotal() {
    var pax = document.getElementById('pax').value;
    var subtotal = 120 * pax;
    document.getElementById('displaysubtotal').innerHTML = subtotal;
    total();
  }

  var show = false;
  var promocode;

  function showpromo() {
    $('#discount').show();
    $('#discountfinal').show();
    show = true;
    var subtotal = document.getElementById("displaysubtotal").innerHTML;
    var finalsubtotal = document.getElementById("displayfinalsubtotal").innerHTML;
    var discount = subtotal * 0.2;
    var finaldiscount = finalsubtotal * 0.2;
    promocode = document.getElementById("promocode").value;
    document.getElementById("displaypromocode").innerHTML = promocode;
    document.getElementById("displaydiscount").innerHTML = discount;
    // promocode = document.getElementById("promocode").value;
    document.getElementById("displayfinalpromocode").innerHTML = promocode;
    document.getElementById("displayfinaldiscount").innerHTML = finaldiscount;
    total();
  }

  function hidepromo() {
    $('#discount').hide();
    $('#discountfinal').hide();
    show = false;
    document.getElementById("displaydiscount").innerHTML = 0;
    document.getElementById("displayfinaldiscount").innerHTML = 0;
    total();
  }

  var finalsubtotal,
      finaltotal,
      finaldiscount;

  function total() {
    finalsubtotal = document.getElementById('displaysubtotal').innerHTML;
    finaldiscount = document.getElementById('displaydiscount').innerHTML;
    finaltotal = finalsubtotal - finaldiscount;
    document.getElementById('displaytotal').innerHTML = "$" + finaltotal;

    var final_subtotal = document.getElementById('displayfinalsubtotal').innerHTML;
    var final_discount = document.getElementById('displayfinaldiscount').innerHTML;
    var final_total = final_subtotal - final_discount;
    document.getElementById('displayfinaltotal').innerHTML = "$" + final_total;

    return finaltotal, finaldiscount, finalsubtotal;
  }
